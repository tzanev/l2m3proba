\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M3ParcRenf}

\let\rond\mathcal

% \solutionstrue

\begin{document}

\hautdepage{TD1 - dénombrement, probabilité et indépendance}


% ==================================
\section{Probabilités élémentaires}
% ==================================

%-----------------------------------
\begin{exo}

  On tire, une à une, sans les remettre dans le paquet, cinq cartes dans un jeu de 52 cartes; chaque succession de cartes  ainsi tirées s'appelle une main.
  \begin{enumerate}
    \item Quel est l'espace de probabilité que vous considérez ?
    \item Quelle est la probabilité qu'une main soit :
    \begin{enumerate*}[label=]
      \item une «couleur» (ou «flush»)\footnote{Main composée que d'une des 4 couleurs : $\spadesuit$, $\heartsuit$, $\clubsuit$ et $\diamondsuit$.} ?
      \item une «quinte flush» ?
      \item un «carré» ?
      \item un «full»\footnote{L'association d'un «brelan» \emph{(3 cartes identiques)} et d'une «paire» \emph{(2 cartes identiques)}.} ?
    \end{enumerate*}
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo} %\label{mot de 4 lettres}

  Soit $p\geq 4$. On construit un mot de $p$ lettres en choisissant au hasard des lettres dans un alphabet constitué de  4 lettres différentes $\{A,T,C,G\}$.
  \begin{enumerate}
    \item Décrire l'espace de probabilité associé à cette expérience.
    \item Quelle est la probabilité que dans  le mot obtenu les 4 lettres de l'alphabet soient présentes ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.7](En attendant le bus)

  Un arrêt de bus est desservi tous les quart d'heures à partir de 7h du matin (inclus). Un passager arrive à l'arrêt à un instant aléatoire de [7h10 ; 7h40] muni de la loi uniforme.

  Quelle est la probabilité qu'il attende moins de 5 min pour un bus ? Plus de 10 min ?

\end{exo}


% -----------------------------------
\begin{exo} (Temps d'attente de Pierre et Paul) %%% extrait du partiel 2004-2005

  Pierre et Paul ont rendez-vous entre 12h et 12h30. Dans un premier temps, on discrétise le temps et on modélise cette situation par
  \[
    \Omega=\{1,2,3,\ldots,30\}^2,
  \]
  le tirage $ \omega=(\omega_1,\omega_2) $ représentant la situation où Pierre arrive $ \omega_1 $ minutes après 12h et où Paul arrive $ \omega_2 $ minutes après 12h. On munit $ \Omega $ de la tribu $ {\rond{P}}(\Omega) $ et on fait l'hypothèse d'équiprobabilité.
  \begin{enumerate}
    \item Quelle est la probabilité de l'événement \enquote{Pierre et Paul arrivent en même temps }?
    \item Calculer la probabilité de l'événement \enquote{Pierre attend plus de 5 minutes}:
    \[
      \ensemble{ (\omega_1,\omega_2) \in \Omega }{ \omega_2 > \omega_1 + 5 }.
    \]
    Quelle est celle de l'événement \enquote{Pierre attend entre 5 et 15 minutes } (ces deux valeurs extrêmes étant exclues) ?
    \item Quelle est la probabilité que Pierre et Paul arrivent avec $k$ minutes de différence (pour $k\in\{0,\dots,29\}$) ?
    \item Quelle autre modélisation aurait-on pu choisir ? Est-ce que cela aurait changé les probabilités des événements considérés ?
  \end{enumerate}
  Nous considérons maintenant que le temps est continu. Nous choisissons la modélisation $\Omega=[0,30]^2$, avec probabilité uniforme. Le tirage $\omega=(\omega_1,\omega_2)$ représente la situation où Pierre arrive $\omega_1$ minutes après 12h et Paul $\omega_2$ minutes après 12h.
  \begin{enumerate}[resume]
    \item Calculer la probabilité de l'événement: «Pierre et Paul arrivent en même temps».
    \item Faire un dessin représentant les événements: «Pierre arrive avant Paul», «Pierre attend plus de 5 minutes».
    \item Calculer la probabilité de l'événement: «Pierre attend plus de 5 minutes», «Pierre attend entre 5 et 15 minutes».
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}[start=3]
    \item L'ensemble $ \Omega=\{1,2,3,\ldots,30\}^2 $ des événements élémentaires compte $ 30^2=900 $ éléments. $ ( \Omega , {\rond{P}}(\Omega) ) $ étant muni de l'équiprobabilité, chaque événement observable $ A $ a pour probabilité:
    \[
      \PP{A}=\frac{\textrm{Card}(A)}{\textrm{Card}(\Omega)}
    \]
    et en particulier l'événement \enquote{Pierre et Paul arrivent en même temps } est de probabilité:
    \[
      \PP{\ensemble{ (\omega_1,\omega_2) \in \Omega }{ \omega_2 = \omega_1 }}
      = \frac{30}{900} = \frac{1}{30}.
    \]
    L'événement \enquote{Pierre attend plus de 5 minutes } correspond à la partie \enquote{triangulaire}  suivante de $ \Omega $ (cf. figure~ref{fig-attente+5}):
    \begin{multline*}
      \ensemble{ (\omega_1,\omega_2) \in \Omega }{ \omega_2 > \omega_1 + 5 }   \\
      = \{ (1,7),(1,8),\cdots,(1,30) \}
      \cup \{ (2,8),(2,9),\cdots,(2,30) \}
      \cup \{ (3,9),\cdots,(3,30) \}
      \cup \cdots                                                        \\
      \cdots
      \cup \{ (23,29),(23,30) \}
      \cup \{ (24,30) \}
    \end{multline*}
    qui est de cardinal $ 24+23+\cdots+1=24\times25/2 $ donc cet événement est de probabilité:
    \[
      \PP{\ensemble{ (\omega_1,\omega_2) \in \Omega }{ \omega_2 > \omega_1 + 5 }}
      = \frac{24\times25/2}{900} = \frac{1}{3}.
    \]
    De même, la probabilité que Pierre attende plus de 14 minutes est:
    \begin{multline*}
      \PP{\ensemble{ (\omega_1,\omega_2) \in \Omega }{ \omega_2 > \omega_1 + 14 }} \\
      = \PP{\{ (1,16),(1,17),\cdots,(1,30) \}
          \cup \{ (2,17),\cdots,(2,30) \}
          \cup \cdots
          \cup \{ (14,29),(14,30) \}
          \cup \{ (15,30) \}}                                               \\
      = \frac{15\times16/2}{900} = \frac{2}{15}.
    \end{multline*}
    La probabilité que Pierre attende entre 5 et 15 minutes (ces deux valeurs extrêmes étant exclues) est la différence des deux (cf. figure~ref{fig-attente+5-15}):
    \[
      \PP{\ensemble{ (\omega_1,\omega_2) \in \Omega }{ \omega_1 + 5 < \omega_2 < \omega_1 + 15 }}
      = \frac{1}{3} - \frac{2}{15}
      = \frac{1}{5}.
    \]
    La variable aléatoire $ T $ égale au temps d'attente du premier qui arrive est définie sur $ \Omega $ par
    \[
      \forall (\omega_1,\omega_2) \in \Omega, \quad
      T(\omega_1,\omega_2) = |\omega_1-\omega_2|.
    \]
    L'ensemble des valeurs qu'elle peut prendre est:
    \[
      T(\Omega) = \{~ 0,1,2,\cdots,29 ~\}
    \]
    et pour $ k \in T(\Omega) $ on a (cf. figure~ref{fig-T=3}):
    \begin{multline*}
      \{~ T=k ~\}
      = \ensemble{ (\omega_1,\omega_2) \in \Omega }{ |\omega_1 - \omega_2|=k }   \\
      = \{~ (1,1+k),(2,2+k),\cdots,(30-k,30) ~\}
        \cup \{~ (1+k,1),(2+k,2),\cdots,(30,30-k) ~\}
    \end{multline*}
    d'où pour $ k \in \{~ 1,2,\cdots,29 ~\} $
    \[
      \PP{T=k}=\frac{\textrm{Card}(\{~ T=k ~\})}{\textrm{Card}(\Omega)}
            =\frac{(30-k)+(30-k)}{900}
            =\frac{2 \times (30-k)}{900}.
    \]
    Pour $ k=0 $, il s'agit de la probabilité que Pierre et Paul arrivent en même temps, qui a déja été calculée:
    \[
      \PP{T=0}
      =\frac{\textrm{Card}(\{~(1,1),(2,2),\cdots,(30,30)~\})}{\textrm{Card}(\Omega)}
      =\frac{1}{30}.
    \]
    On vérifie facilement que les $ \PP{T=k} $ trouvés sont bien positifs et de somme égale à 1 (par exemple en utilisant la formule de la question 1).
  \end{enumerate}
  \begin{enumerate}[start=7]
  \item
    \begin{itemize}
      \item 0 bien sûr.
      \item On calcule l'aire de la surface: $25^2/2/30^2=25/72$ et 2/9.
    \end{itemize}
  \end{enumerate}
\end{solution}


% -----------------------------------
\begin{exo} (Découpe de spaghetti)%\label{spagghetti}

  On découpe \enquote{au hasard}, un segment de longueur $l$ en trois morceaux. On veut savoir si on peut tracer un triangle avec les trois morceaux.
  \begin{enumerate}
    \item Décrire d'espace de probabilité $\Omega$ associé, ainsi que la tribu et la probabilité.
    \item On modélise le spaghetti par le segment $[0,l]$. Le point où l'on coupe le spaghetti pour la première fois est noté $x$, le point où l'on coupe le spaghetti pour la deuxième fois $y$. On peut avoir $x>y$.
    \item Quand peut-on faire un triangle avec les trois morceaux ?
    \item On peut représenter le couple $(x,y)$ par un point de $\mathbb{R}^2$. Représenter sur un dessin les couples qui permettent de faire un triangle.
    \item Calculer la probabilité de pouvoir faire un triangle avec les trois morceaux du spaghetti.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}[start=5]
  \item Il faut d'abord modéliser l'événement. On place uniformément deux points sur $[0,l]$. Donc $\Omega=[0,l]^2$, $\rond{F}=\mathbb{B}(\Omega)$, proba uniforme. Il faut que la somme des longueurs des deux plus petits morceaux soit supérieure ou égale à la longueur du plus grand. On note $(x,y)$ les deux points. Si $x<y$, on a trois côtés: $x$, $y-x$, $l-y$. Il faut que
  \begin{itemize}
    \item $x+y-x\geq l-y$ donc $y\geq l/2$.
    \item $x+l-y\geq y-x$ donc $y\leq x+l/2$
    \item $y-x+l-y\geq x$ donc $x\leq l/2$. On trace cette zone, et la zone symétrique par rapport à la droite $y=x$.
  \end{itemize}
  Il ne reste plus qu'à calculer l'aire: 1/4.
  \end{enumerate}
\end{solution}


% ==================================
\section{Indépendance et conditionnement}
% ==================================

%-----------------------------------
\begin{exo}

  On lance deux dés et on considère les événements :
  \begin{align*}
    A = & \{\text{le résultat du premier dé est impair}\},\\
    B = & \{\text{le résultat du second dé est pair}\},\\
    C = & \{\text{les résultats des deux dés sont de même parité}\}.
  \end{align*}
  Étudier l'indépendance deux à deux des événements $A$, $B$ et $C$,
  puis l'indépendance mutuelle (indépendance de la famille) $A,B,C$.

\end{exo}


% -----------------------------------
\begin{exo} %\label{révision indépendances}

  Pour chacune des assertions suivantes, donner soit une preuve, soit un contre-exemple.
  \begin{enumerate}
    \item Si $A$ et $B$ sont deux événements indépendants et incompatibles alors l'un des deux événements au moins est de probabilité nulle.
    \item Si l'un des événements $A$ ou $B$ est de probabilité nulle alors $A$ et $B$  sont indépendants et incompatibles.
    \item Si un événement $A$ est indépendant d'un événement $B$ et si $C$ est un événement tel que $C\subset B$ alors $A$ est indépendant de $C$.
    \item Si un événement $A$ est indépendant d'un événement $B$ et d'un événement $C$, alors il est indépendant de $B\cup C$.
 \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}

  On cherche une girafe qui, avec une probabilité $p/7$, se trouve dans l'un des quelconques des $7$ étages d'un immeuble, et avec probabilité $1-p$ hors de l'immeuble. On a exploré en vain les $6$ premiers étages. Quelle est la probabilité qu'elle habite au  septième étage ?
\end{exo}

%-----------------------------------
\begin{exo}

  Un joueur de tennis a une probabilité de $40\%$ de passer sa première balle de service. S'il échoue, sa probabilité de passer sa deuxième balle est $70\%$. Lorsque sa première balle de service passe, sa probabilité de gagner le point est $80\%$, tandis que sa probabilité de gagner le point lorsqu'il passe sa deuxième balle de service n'est plus que $50\%$.

  \begin{enumerate}
    \item Calculer la probabilité qu'il fasse une double faute.
    \item Calculer la probabilité qu'il perde le point sur son service.
    \item Sachant qu'il a perdu le point, quelle est la probabilité que ce soit sur une double faute ?
  \end{enumerate}

\end{exo}


% -----------------------------------
\begin{exo} (Une inégalité injustement méconnue) %\label{une inégalité méconnue}

  Sur l'espace probabilisé $(\Omega,{\rond{F}},\mathbb{P})$, on note $A$ un événement quelconque et $B$ un événement tel que $0<\PP{B}<1$.
  \begin{enumerate}
    \item Montrez que
    \begin{equation}\label{indeq}
      \abs{\PP{A\cap B}-\PP{A}\PP{B}} \leq \frac{1}{4}\abs{\PP{A\mid B} - \PP{A\mid B^c}}.\tag*{$\circledast$}
    \end{equation}
    \begin{indication}
      Commencez par exprimer $\PP{A\mid B} - \PP{A\mid B^c}$ en fonction des seules probabilités $\PP{A\cap B}$, $\PP{A}$, $\PP{B}$.
    \end{indication}
    \item Dans quels cas \ref{indeq} est-elle une égalité ?
    \item Que donne l'inégalité~\ref{indeq} lorsque $A\subset B$ ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.7](Indépendance) %\label{indépendance}

  Peut-il exister $n$ événements indépendants de même probabilité $p$ dont la réunion soit l'espace $\Omega$ tout entier ?
\end{exo}

% -----------------------------------
\begin{exo} (Loi de succession de Laplace) %\label{loi de succession de Laplace}

  On dispose de $(N+1)$ boîtes numérotées de $0$ à $N$. La $k$-ième boîte contient $k$ boules rouges et $(N-k)$ boules blanches. On choisit une boîte au hasard et on fait, dans cette boîte, $n$ tirages avec remise.
  \begin{enumerate}
    \item Sachant que le tirage  est effectué dans la $k$-ième boîte, quelle est la probabilité de tirer  $n$ fois de suite une boule rouge ?
    \item Sachant qu'on a tiré une boule rouge, quelle est la probabilité que le tirage a été effectué dans la $k$-ième boîte ?
    \item Démontrer que la probabilité de tirer $n$ fois de suite une boule rouge est :
    \[
      \frac{1+2^n+3^n+\cdots +N^n}{N^n(N+1)}
    \]
    et calculer sa limite quand $N$ tend vers l'infini.\\
    \begin{indication}
      Utiliser la définition de l'intégral de Riemann pour déterminer la limite.
    \end{indication}
    \item Calculer la probabilité $p_{N,n}$ de tirer une boule rouge la $(n+1)^{\textrm{ième}}$ fois, sachant qu'on vient de tirer $n$ boules rouges de suite. Quelle est sa limite quand $N$ tend vers l'infini ?
  \end{enumerate}
\end{exo}

\end{document}
