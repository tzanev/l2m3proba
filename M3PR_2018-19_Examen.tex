\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M3ParcRenf}

% pour voir les solutions il faut enlever le commentaire de la ligne suivante
% \solutionstrue

\begin{document}

% ==================================
\hautdepage{\vspace{-3mm}
  \sisujet{Examen}\sisolutions{Solutions de l'examen}\\
  \normalfont\normalsize
  10 décembre 2018\\
  {[ durée: 1 heure ]}
}
% ==================================
\sisujet{
  \attention~\textbf{Aucun document n'est autorisé. Les calculatrices sont autorisés.}
  \vspace{7mm}
}

% -----------------------------------
\begin{exo} (Variables)

  Soit $X$ une variable uniforme sur $[-1,2]$.
  \begin{enumerate}
    \item Donner une densité et la fonction de répartition de $X$.
    \item Calculer les espérances $\EE{X}$ et $\EE{X^2}$. En déduire la variance $\VV{X}$.
    \item Pour $a\in[0,1]$ on pose $Z=\max\{X,a\}$.
    \begin{enumerate}
      \item Quelle est la loi de $Z$ ?
      \item Est-ce une variable à densité ? Si oui, en déterminer une.
      \item Calculer la moyenne de $Z$. Pour quelle valeur de $a$ la moyenne $\EE{Z}$ est maximale ?
    \end{enumerate}
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item La densité d'une variable uniforme sur $[a,b]$ est $\frac{1}{b-a}\ind_{[a,b]}(t)$, donc $\rho_{X}(t) = \frac{1}{3}\ind_{[-1,2]}(t)$.\\
    La fonction de répartition d'une variable uniforme sur $[a,b]$ est
    \[
      t \mapsto
      \begin{cases}
        0, &\text{ si } t \leq a\\
        \frac{t-a}{b-a}, &\text{ si } t \in [a,b]\\
        1, &\text{ si } t \geq b\\
      \end{cases}
      \text{,\quad ainsi dans notre cas \quad}
      F_X(t) =
      \begin{cases}
        0, &\text{ si } t \leq -1\\
        \frac{t+1}{3}, &\text{ si } t \in [-1,2]\\
        1, &\text{ si } t \geq 2\\
      \end{cases}.
    \]
    \item On sait que l'espérance d'une variable uniforme sur $[a,b]$ est $\frac{a+b}{2}$, mais on peut la retrouver dans notre cas
    \[
      \EE{X} = \frac{1}{3}\int_{-1}^{2}t\,dt = \frac{1}{3}\left[ \frac{t^2}{2} \right]_{-1}^{2} = \frac{1}{2}
    \]
    De même on calcule
    \[
      \EE{X^2} = \frac{1}{3}\int_{-1}^{2}t^2\,dt = \frac{1}{3}\left[ \frac{t^3}{3} \right]_{-1}^{2} = \frac{1}{3}\frac{8-(-1)}{3} = 1.
    \]
    Ainsi
    \[
      \VV{X^2} = \EE{X^2} - \EE{X}^2 = 1 - \frac{1}{4} = \frac{3}{4}.
    \]
    \item Pour $a\in[0,1]$ on pose $Z=\max\{X,a\}$ ?
    \begin{enumerate}
      \item Comme $X \in [0,2]$, nous avons $Z=\max\{X,a\} \in [a,2]$ $\implies$ $F(t)\equiv0$ pour $t<a$ et $F(t)\equiv1$ pour $t\geq2$. Ainsi pour connaître la loi de $Z$ il suffit de connaître la fonction de répartition $F_{Z}(t) = \PP{Z \leq t}$ pour $t \in [a,2]$. Mais pour $t \geq a$ nous avons $\max\{X,a\} \leq t \iff X \leq t $ et donc $F_{Z}(t) = \PP{X \leq t} = F_{X}(t)$ pour $t \in [a,2]$. Ainsi
      \[
        F_Z(t) =
          \begin{cases}
            0, &\text{ si } t < a\\
            \frac{t+1}{3}, &\text{ si } t \in [a,2]\\
            1, &\text{ si } t \geq 2\\
          \end{cases}.
       \]
      \item La fonction $F_Z(t)$ n'est pas continue en $a$, donc $Z$ n'a pas de densité.
      \item $\EE{Z} = \int_{\mathbb{R}} \max\{t,a\} \rho_{X}(t) dt = \frac{1}{3}\int_{-1}^2 \max\{t,a\} dt = \frac{1}{3}\int_{-1}^a a dt + \frac{1}{3}\int_{a}^2 t dt = \frac{a}{3}(a+1) + \frac{1}{3}\left[ \frac{t^2}{2} \right]_{a}^2 $. Ainsi après simplification nous trouvons $\EE{Z} = \frac{2}{3} + \frac{a}{3} + \frac{a^2}{6}$, qui est croissante sur $[0,1]$ (qui était prévisible), donc est maximale pour $a=1$, et elle vaut dans ce cas $\EE{Z} = \frac{7}{6}$.
    \end{enumerate}
  \end{enumerate}
\end{solution}


%-----------------------------------
\begin{exo} (Événements)

  On dispose d'un dé bleu et d'un dé rouge \emph{équilibrés} et on effectue trois lancers de cette paire de dés.
  \begin{enumerate}
    \item Donner un ensemble $\Omega$ qui muni de la loi uniforme modélise cet événement aléatoire.
    \item Quelle est la probabilité qu'on obtienne au moins une paire\footnote{«paire» = les deux dés ont la même valeur} lors des trois lancés ?
    \item Quelle est la probabilité que lors de chacun des trois lancés la différence des deux dés soit de $1$ ?
    \item Est-ce que les événements «lors du premier lancé la somme des deux dés est paire» et «lors du premier lancé la différence des deux dés est impaire» sont indépendants ?
  \end{enumerate}

\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Soit $\mathcal{S}=\left\{ 1,\dots,6 \right\}$ qui encode le résultat d'un dé lors d'un lancé. Alors $\mathcal{S}^{2} = \ensemble[\big]{(r_1,b_1)}{r_1,b_1 \in \mathcal{S}}$ encode le résultat de deux dés lors d'un lancé\footnote{On a choisi «r» pour «rouge» et «b» pour «bleu».}. Et finalement
    \[
       \Omega = \left( \mathcal{S}^{2} \right)^3 = \ensemble[\big]{\big( (r_1,b_1),(r_2,b_2),(r_3,b_3) \big)}{r_i,b_i \in \mathcal{S}}
     \]
    modélise les trois lancés des deux dés, avec $| \Omega | = 6^{6} = 46656$.
    \item Soit $A_{i}$ l'événement «c'est une paire lors du $i$-ème lancé». Les $A_{i}$ sont indépendants car les lancés le sont. Alors $\PP{A_i}=\frac{6}{36}=\frac{1}{6}$ et donc  $\PP{\overline{A_i}}=\frac{5}{6}$. Ainsi $\PP{\text{«au moins une paire»}} = 1-\PP{\text{«aucune paire»}} = 1- \left(\frac{5}{6}\right)^3 = \frac{91}{216} \approx 42\%$.
    \item Soit $D_{i}$ l'événement «les deux dés diffèrent de $1$ lors du $i$-ème lancé». Les $D_{i}$ sont indépendants car les lancés le sont. Comme il y a $10$ résultats où les deux dés diffèrent de $1$, $\PP{D_i}=\frac{10}{36}=\frac{5}{18}$. Ainsi $\PP{\text{«les dés diffèrent de $1$ les trois fois»}} = \left(\frac{5}{18}\right)^3 = \frac{125}{5832} \approx 2\%$.
    \item Comme $A$~: «la somme des deux dés est paire» $\iff$ «les deux dés ont la même parité», et $B$~: «la différence des deux dés est impaire» $\iff$ «les deux dés n'ont pas la même parité», ces deux événements sont complémentaires. Et comme ils sont possibles, ils sont dépendants : $\PP{A \cap B} = 0 \neq \PP{A}\PP{B}$.
  \end{enumerate}

\end{solution}

\end{document}
