# [l2m3proba](https://gitlab.univ-lille.fr/tzanev/l2m3proba) / [page web](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/)

Les tds du module M3 - «Probabilités» du Parcours renforcé L2 Physique de l'Université de Lille.

## 2018/19

Vous pouvez récupérer [ce dépôt](https://gitlab.univ-lille.fr/tzanev/l2m3proba) de deux façons faciles :

- en téléchargeant l'archive [zip](https://gitlab.univ-lille.fr/tzanev/l2m3proba/-/archive/master/parcrenf-master.zip) qui contient la dernière version des fichiers,
- récupérer l'intégralité de ce dépôt, y compris l'historique des modifications, en utilisant `git` avec la commande

  ~~~~~~~
  git clone https://gitlab.univ-lille.fr/tzanev/l2m3proba.git
  ~~~~~~~

Dans [ce dépôt](https://gitlab.univ-lille.fr/tzanev/l2m3proba) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX lors du dépot et la version CI) suivants :

- Feuille de TD n°1 [[tex](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_TD1.tex)] [[pdf](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_TD1.pdf)] ou
  [*recompiler avec [latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.univ-lille.fr/tzanev/l2m3proba.git&command=xelatex&target=M3PR_2018-19_TD1.tex)*]
- Feuille de TD n°2 [[tex](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_TD2.tex)] [[pdf](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_TD2.pdf)] ou
  [*recompiler avec [latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.univ-lille.fr/tzanev/l2m3proba.git&command=xelatex&target=M3PR_2018-19_TD2.tex)*]
- Feuille de TD n°3 [[tex](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_TD3.tex)] [[pdf](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_TD3.pdf)] ou
  [*recompiler avec [latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.univ-lille.fr/tzanev/l2m3proba.git&command=xelatex&target=M3PR_2018-19_TD3.tex)*]
- Examen [[tex](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_Examen.tex)]
  - Sujet [[pdf](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_Examen.pdf)] ou
  [*recompiler avec [latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.univ-lille.fr/tzanev/l2m3proba.git&command=xelatex&target=M3PR_2018-19_Examen.tex)*]
  - Solutions [[pdf](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3PR_2018-19_Examen_solutions.pdf)]


Pour compiler ces feuilles de td vous avez besoin de la feuille de style [M3ParcRenf.sty](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/M3ParcRenf.sty) ainsi que le [logo du déprtement](https://tzanev.gitlabpages.univ-lille.fr/l2m3proba/ul-fst-math_noir.pdf).
